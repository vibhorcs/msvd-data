import json

def load():
    f = open('./config.json', 'r')
    config = json.loads(f.read().strip())
    f.close()
    return config
