import csv
import json
import re
import os
import urllib.error
import config
from tqdm import tqdm
from pytube import YouTube
import pytube.exceptions

CONFIG = config.load()
CORPUS_PATH = '%s/corpus' % CONFIG['data_path']
VIDEO_FILENAME_PATH = '%s/video-filename' % CONFIG['data_path']
VIDEO_PATH = '%s/video' % CONFIG['data_path']

def init_path():
    for path in [CORPUS_PATH, VIDEO_FILENAME_PATH, VIDEO_PATH]:
        if not os.path.exists(path):
            os.makedirs(path)

def get_url(youtube_id):
    return 'http://youtube.com/watch?v=%s' % youtube_id

def get_youtube_ids():
    corpus_file_path = '%s/corpus.csv' % CORPUS_PATH
    youtube_ids = set()
    with open(corpus_file_path, 'r', encoding='utf-8') as f:
        f.readline()
        corpus_reader = csv.reader(f, delimiter=',', quotechar='"')
        for row in corpus_reader:
            if not row:
                continue
            youtube_ids.add(row[0])
    return list(youtube_ids)

def get_resolution(stream):
    if stream.resolution is None:
        return 0
    elif re.match(r'\d+[ip]', stream.resolution) is None:
        return 0
    else:
        return int(stream.resolution[:-1])

def get_stream_sort_key(stream):
    if stream.resolution is None:
        return (0, 0)
    elif re.match(r'\d+[a-z]', stream.resolution) is None:
        return (0, 0)
    else:
        return (int(stream.resolution[:-1]), 0 if stream.resolution.endswith('p') else 1)

def get_target_stream(streams):
    sorted_streams = [(stream, get_resolution(stream)) for stream in sorted(streams, key=get_stream_sort_key) if stream.includes_video_track]
    if len(sorted_streams) == 0:
        return None
    target_stream, _ = sorted_streams[-1]
    for stream, resolution in sorted_streams:
        if resolution >= 360:
            target_stream = stream
            break
    return target_stream

def get_filename_path(youtube_id):
    return '%s/%s.json' % (VIDEO_FILENAME_PATH, youtube_id)

def save_filename(youtube_id, filename):
    with open(get_filename_path(youtube_id), 'w') as f:
        path_data = json.dumps({'filename': filename})
        f.write(path_data)

def save_error(youtube_id, error_code):
    with open(get_filename_path(youtube_id), 'w') as f:
        path_data = json.dumps({'error_code': error_code})
        f.write(path_data)

def main():
    init_path()
    youtube_ids_for_download = []
    for youtube_id in get_youtube_ids():
        if not os.path.exists(get_filename_path(youtube_id)):
            youtube_ids_for_download.append(youtube_id)
    progress_bar = tqdm(youtube_ids_for_download)
    for youtube_id in progress_bar:
        progress_bar.set_description('Downloading %s' % youtube_id)
        try:
            yt = YouTube(get_url(youtube_id))
        except pytube.exceptions.RegexMatchError:
            save_error(youtube_id, 'match_error')
            continue
        except pytube.exceptions.AgeRestrictionError:
            save_error(youtube_id, 'blocked')
            continue
        except urllib.error.HTTPError:
            save_error(youtube_id, 'http_error')
            continue
        streams = yt.streams.filter(file_extension='mp4').all()
        stream = get_target_stream(streams)
        if stream is None:
            save_error(youtube_id, 'no_video_stream')
            continue
        filename = stream.default_filename
        stream.download(output_path=VIDEO_PATH)
        save_filename(youtube_id, filename)

if __name__ == '__main__':
    main()
