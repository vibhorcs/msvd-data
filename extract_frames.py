import csv
import json
import os
import config
from tqdm import tqdm

CONFIG = config.load()
CORPUS_PATH = '%s/corpus' % CONFIG['data_path']
VIDEO_FILENAME_PATH = '%s/video-filename' % CONFIG['data_path']
VIDEO_PATH = '%s/video' % CONFIG['data_path']
VIDEO_FRAME_IMAGE_PATH = '%s/video-frame-image' % CONFIG['data_path']

def init_path():
    for path in [CORPUS_PATH, VIDEO_FILENAME_PATH, VIDEO_PATH, VIDEO_FRAME_IMAGE_PATH]:
        if not os.path.exists(path):
            os.makedirs(path)

def get_youtube_ids():
    corpus_file_path = '%s/corpus.csv' % CORPUS_PATH
    youtube_ids = set()
    with open(corpus_file_path, 'r', encoding='utf-8') as f:
        f.readline()
        corpus_reader = csv.reader(f, delimiter=',', quotechar='"')
        for row in corpus_reader:
            if not row:
                continue
            youtube_ids.add(row[0])
    return list(youtube_ids)

def get_frame_path(youtube_id):
    return '%s/video_%s' % (VIDEO_FRAME_IMAGE_PATH, youtube_id)

def get_video_path(youtube_id):
    with open('%s/%s.json' % (VIDEO_FILENAME_PATH, youtube_id)) as f:
        filename_obj = json.loads(f.read().strip())
        if 'filename' in filename_obj:
            return '%s/%s' % (VIDEO_PATH, filename_obj['filename'])
        else:
            return None

def remove_frames(youtube_id):
    frame_path = get_frame_path(youtube_id)
    for filename in os.listdir(frame_path):
        file_path = os.path.join(frame_path, filename)
        os.remove(file_path)

def extract_frames(youtube_id, fps=1, width=320, height=240):
    video_path = get_video_path(youtube_id)
    if video_path is None:
        return False
    frame_path = '%s/video_%s' % (VIDEO_FRAME_IMAGE_PATH, youtube_id)
    if not os.path.exists(frame_path):
        os.makedirs(frame_path)
    os.system('ffmpeg -nostats -loglevel 0 -i "%s" -r %d -s %dx%d -f image2 %s/%%d.jpg' % (video_path, fps, width, height, frame_path))
    f = open('%s/done.json' % frame_path, 'w')
    f.write(json.dumps({'result': 'done'}))
    f.close()
    return True

def main():
    init_path()
    youtube_ids_for_extraction = []
    for youtube_id in get_youtube_ids():
        frame_path = get_frame_path(youtube_id)
        if not os.path.exists(frame_path):
            youtube_ids_for_extraction.append(youtube_id)
        elif not os.path.exists('%s/done.json' % frame_path):
            youtube_ids_for_extraction.append(youtube_id)
            remove_frames(youtube_id)
    progress_bar = tqdm(youtube_ids_for_extraction)
    for youtube_id in progress_bar:
        extract_frames(youtube_id)

if __name__ == '__main__':
    main()
